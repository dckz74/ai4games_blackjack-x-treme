# AI4Games_BlackJack X-Treme 

## Download-Guide
- das gesamte Repository downloaden (Links neben dem blauen Feld "Clone") und entpacken
- im Ordner "BlackJack X-Treme" die BlackJack X-Treme.exe starten
- Hinweis: Windows vertraut dieser Datei nicht - die macht aber nichts Schlimmes, versprochen! :innocent:

## GameLog Files
- nach jedem Spiel wird ein Log erfasst
- falls ihr das Spiel beenden wollt, bitte **NICHT** das Spiel mit "Alt+F4" verlassen! Entweder oben den "Quit game"-Knopf benutzen ODER den "Exit?"-Knopf am Ende eines Spiels 
- die Logs werden dann in folgenden Ordnern gespeichert:
    - **WINDOWS**: %userprofile%\AppData\LocalLow\ElDa\BlackJack X-Treme
    - **LINUX**: $XDG_CONFIG_HOME/unity3d **ODER** $HOME/.config/unity3d
    - **MAC**: ~/Library/Application Support/ElDa/BlackJack X-Treme
- die Logs entweder über private Kanäle an uns oder über folgende E-Mail-Adressen:
    - david.koschitzki@mni.thm.de
    - elena.abraham@mni.thm.de
- Spielt ruhig ab und an im Laufe der Wochen wieder und schickt sie bis spätestens zum 25.07.2021 an uns! ♥ 

## Spielregeln
- abgeänderte Version von Black Jack
- jeder Spieler hat 5 Leben
- Karten von 1 bis 11 sind **EINMALIG im Deck, BEIDE Spieler ziehen aus DEMSELBEN Deck**
- Die erstgezogenen Karten beider Spieler sind **NUR für den Spieler selbst sichtbar, NICHT für den Gegner**
- Jeder Spieler kann, wenn er dran ist, folgende Optionen wählen: Trumpfkarten spielen, Karte ziehen (zufällig aus dem Stapel gezogen), keine Karte ziehen
- den allerersten Zug betätigt immer der Spieler, danach wechselt es nach jeder Runde
- Während ein Spieler dran ist, können **beliebig viele Trumpfkarten gespielt** werden
- Der eigene Zug wird entweder mit "Draw a card" oder "Skip turn" beendet
- Wenn beide Spieler nacheinander **keine Karten ziehen UND keine Trumpfkarte spielen**, dann wird geschaut, wer die Runde gewonnen hat
- Spieler, der näher an 21 ist, gewinnt - anderer Spieler verliert Leben
- Spieler über 21 verliert
- Unentschieden: beide Spieler über 21 ODER beide Spieler gleiche Punktzahl
- **_BESONDERHEIT_**: Trumpfkarten, die ein wenig mehr Komplexität schaffen

## Trumpfkarten
- Zu Beginn erhält jeder Spieler eine zufällige Trumpfkarte
- Im restlichen Spielverlauf erhält man mit einer Wahrscheinlichkeit von 65 % zufällige Trumpfkarten, **wenn man eine Karte zieht**
- Während man am Zuge ist, dürfen beliebig viele Trumpfkarten gespielt werden
- Es können sich maximal 5 Trumpfkarten im Besitz eines Spielers befinden, danach werden keine neuen Trumpfkarten mehr gezogen
- Hat ein Spieler über 30 Punkte, so erhält dieser auch keine Trumpfkarten mehr
- In diesem Spiel existieren **8 verschiedene** Trumpfkarten:
    - Tausche deine zuletzt gezogene Karte mit dem Gegner
    - Sieg bei 24 (wenn "Sieg bei 27" gespielt wurde, hat diese Trumpfkarte keine Funktion)
    - Sieg bei 27
    - lege deine zuletzt gezogene Karte zurück und mische das Deck
    - Ziehe die Karte 3 aus dem Deck, falls sie noch drin ist
    - Ziehe die Karte 4 aus dem Deck, falls sie noch drin ist
    - Ziehe die Karte 5 aus dem Deck, falls sie noch drin ist
    - Ziehe die Karte 6 aus dem Deck, falls sie noch drin ist

